//
//  ForgotPasswordViewController.swift
//  Dakoni
//
//  Created by mrinal khullar on 1/4/17.
//  Copyright © 2017 brst. All rights reserved.
//

import UIKit

class ForgotPasswordViewController: UIViewController,UITextFieldDelegate {

    @IBOutlet var enterEmailText: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        displayTextFields()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    func displayTextFields()
    {
        
        enterEmailText.backgroundColor = UIColor.white
        enterEmailText.layer.borderColor = UIColor.init(colorLiteralRed: 180/255, green: 180/255, blue: 180/255, alpha: 1.0).cgColor
        enterEmailText.layer.borderWidth = 1.0
        enterEmailText.layer.cornerRadius = 9
        
        let leftView = UILabel(frame: CGRect(x: CGFloat(10), y: CGFloat(0), width: CGFloat(12), height: CGFloat(26)))
        leftView.backgroundColor = UIColor.clear
        
        enterEmailText.leftView = leftView
        enterEmailText.leftViewMode = .always
        enterEmailText.contentVerticalAlignment = .center

    }
    @IBAction func backButtonAction(_ sender: Any) {
    _ = self.navigationController?.popViewController(animated: true)
        
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        textField.resignFirstResponder()
        return true
    }
}
