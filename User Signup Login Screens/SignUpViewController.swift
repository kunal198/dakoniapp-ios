//
//  SignUpViewController.swift
//  Dakoni
//
//  Created by mrinal khullar on 1/4/17.
//  Copyright © 2017 brst. All rights reserved.
//

import UIKit

class SignUpViewController: UIViewController,UITextFieldDelegate {

    
    @IBOutlet var firstnameText: UITextField!
    @IBOutlet var lastnameText: UITextField!
    @IBOutlet var emailText: UITextField!
    @IBOutlet var phoneNumberText: UITextField!
    @IBOutlet var passwordText: UITextField!
    @IBOutlet var confirmPasswordText: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
     // displayTextFields()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func backBtnAction(_ sender: Any) {
        _ = self.navigationController?.popViewController(animated: true)

    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool
{
    textField.resignFirstResponder()
    return true
    }
}
