//
//  LoginVewController.swift
//  Dakoni
//
//  Created by brst on 1/9/17.
//  Copyright © 2017 brst. All rights reserved.
//

import UIKit

class LoginVewController: UIViewController,UITextFieldDelegate {

    @IBOutlet weak var password_txt: UITextField!
    
    @IBOutlet weak var textField_Txt: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
     
    }

    @IBAction func backBtnAction(_ sender: Any) {
        _ = self.navigationController?.popViewController(animated: true)
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    @IBAction func loginBtn_Action(_ sender: Any) {
    }

    @IBAction func forgotBtnAction(_ sender: Any) {
        let ForgotPasswordController = self.storyboard?.instantiateViewController(withIdentifier: "ForgotPasswordViewController") as? ForgotPasswordViewController
        self.navigationController?.pushViewController(ForgotPasswordController!, animated: true)

    }
    
    @IBAction func fbLoginBtnAction(_ sender: Any) {
        
    }
    
    @IBAction func signUpBtnAction(_ sender: Any) {
        
        let signupViewController = self.storyboard?.instantiateViewController(withIdentifier: "SignUpViewController") as? SignUpViewController
        self.navigationController?.pushViewController(signupViewController!, animated: true)
        
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
 }
