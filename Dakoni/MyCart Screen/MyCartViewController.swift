//
//  MyCartViewController.swift
//  Dakoni
//
//  Created by brst on 1/5/17.
//  Copyright © 2017 brst. All rights reserved.
//

import UIKit

class MyCartViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    // Data model: These strings will be the data for the table view cells
    @IBOutlet weak var slideTableVew: UITableView!
    @IBOutlet weak var slideVew: UIView!
    let productImageArray: [String] = ["Headphones.png", "product-1.png", "product-2.png", "product-3.png"]
    
    // cell reuse id (cells that scroll out of view can be reused)
    let cellReuseIdentifier = "Cell"
    
    @IBOutlet weak var tebleView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func crossBtnaction(_ sender: Any) {
        UIView.animate(withDuration: 0.4, delay: 0.0, options: UIViewAnimationOptions.transitionFlipFromLeft, animations: { () -> Void in
            
            self.slideVew.frame.origin.x = -(self.slideVew.frame.size.width)
            
        }, completion: { (finished: Bool) -> Void in
            
            // you can do this in a shorter, more concise way by setting the value to its opposite, NOT value
            
        })

    }
    
    @IBAction func slideButtonAction(_ sender: Any) {
        UIView.animate(withDuration: 0.4, delay: 0.0, options: UIViewAnimationOptions.transitionFlipFromLeft, animations: { () -> Void in
            
            self.slideVew.frame.origin.x = 0
            
        }, completion: { (finished: Bool) -> Void in
            // you can do this in a shorter, more concise way by setting the value to its opposite, NOT value
        })

    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
            if tableView.tag == 1
            {
            return productImageArray.count;
            }
            else{
              return 9
            }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView.tag == 1
        {
            let cell:TableViewCell = self.tebleView.dequeueReusableCell(withIdentifier: "Cell")! as UITableViewCell as! TableViewCell
            let image = UIImage(named: productImageArray[indexPath.row]) as UIImage?
            cell.item_Category_imgVew.image = image
            return cell
        }
        else{
            var cell:UITableViewCell = UITableViewCell()
            cell = self.slideTableVew.dequeueReusableCell(withIdentifier: String(indexPath.row))! as UITableViewCell
            return cell
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        
        if tableView.tag == 2 {
            return 40.0
        }
        else{
            return 144.0
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if tableView.tag == 1 {
            
        }
        else
        {
            if indexPath.row == 0 {
                let homeViewController = self.storyboard?.instantiateViewController(withIdentifier: "ViewController") as? ViewController
                self.navigationController?.pushViewController(homeViewController!, animated: true)
                
            }
            else if indexPath.row == 1{
                let searchViewController = self.storyboard?.instantiateViewController(withIdentifier: "SearchViewController") as? SearchViewController
                self.navigationController?.pushViewController(searchViewController!, animated: true)
            }
            else if indexPath.row == 2{
                
                let LoginController = self.storyboard?.instantiateViewController(withIdentifier: "LoginVewController") as? LoginVewController
                self.navigationController?.pushViewController(LoginController!, animated: true)
                
            }
            else if indexPath.row == 3{
                let myProfileViewController = self.storyboard?.instantiateViewController(withIdentifier: "MyProfileViewController") as? MyProfileViewController
                self.navigationController?.pushViewController(myProfileViewController!, animated: true)
            }
            else if indexPath.row == 4{
                let myCartViewController = self.storyboard?.instantiateViewController(withIdentifier: "MyCartViewController") as? MyCartViewController
                self.navigationController?.pushViewController(myCartViewController!, animated: true)
            }
            else if indexPath.row == 5{
                let aboutUsViewController = self.storyboard?.instantiateViewController(withIdentifier: "AboutUsViewController") as? AboutUsViewController
                self.navigationController?.pushViewController(aboutUsViewController!, animated: true)
            }
            else if indexPath.row == 6{
                let contactUsViewController = self.storyboard?.instantiateViewController(withIdentifier: "ContactUsViewController") as? ContactUsViewController
                self.navigationController?.pushViewController(contactUsViewController!, animated: true)
            }
            else if indexPath.row == 7{
                let viewController = self.storyboard?.instantiateViewController(withIdentifier: "ViewController") as? ViewController
                self.navigationController?.pushViewController(viewController!, animated: true)
            }
            
            print("You selected cell #\(indexPath.row)!")
        }
        
    }


    @IBAction func removeItemButtonAction(_ sender: Any) {
    }
   
    @IBAction func checkoutOutBtnAction(_ sender: Any) {
        
        let shippingController = self.storyboard?.instantiateViewController(withIdentifier: "shippingInformationViewController") as? shippingInformationViewController
        self.navigationController?.pushViewController(shippingController!, animated: true)
    }

    @IBAction func continueShoppingBtn_Action(_ sender: Any) {
        
    }
}
