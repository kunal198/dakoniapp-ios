//
//  shippingInformationViewController.swift
//  Dakoni
//
//  Created by brst on 1/6/17.
//  Copyright © 2017 brst. All rights reserved.
//

import UIKit

class shippingInformationViewController: UIViewController, UIScrollViewDelegate, UITextFieldDelegate {

    @IBOutlet weak var nextBtn_out: UIButton!
    @IBOutlet weak var countryBackGroundVew: UIView!
    @IBOutlet weak var phoneNuber_txt: UITextField!
    @IBOutlet weak var zipCodeView: UIView!
    @IBOutlet weak var cityView: UIView!
    @IBOutlet weak var countryView: UIView!
    @IBOutlet weak var shippingDetailBackgroundView: UIView!
    @IBOutlet weak var scrollView: UIScrollView!
    var tap = UITapGestureRecognizer()
    override func viewDidLoad() {
        super.viewDidLoad()

        
        countryBackGroundVew.layer.cornerRadius = 5.0
        countryBackGroundVew.layer.borderWidth = 0.3
        countryBackGroundVew.layer.borderColor = UIColor.lightGray.cgColor
        countryBackGroundVew.clipsToBounds = true

        countryView.layer.cornerRadius = 5.0
        countryView.layer.borderWidth = 0.3
        countryView.layer.borderColor = UIColor.lightGray.cgColor
        countryView.clipsToBounds = true

        cityView.layer.cornerRadius = 5.0
        cityView.layer.borderWidth = 0.3
        cityView.layer.borderColor = UIColor.lightGray.cgColor
        cityView.clipsToBounds = true

        zipCodeView.layer.cornerRadius = 5.0
        zipCodeView.layer.borderWidth = 0.3
        zipCodeView.layer.borderColor = UIColor.lightGray.cgColor
        zipCodeView.clipsToBounds = true
        
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        self.scrollView.contentSize = CGSize(width: self.scrollView.contentSize.width, height: shippingDetailBackgroundView.frame.origin.x + shippingDetailBackgroundView.frame.size.height + 50)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func nextview_BtnAction(_ sender: Any) {
        
        let checkout = self.storyboard?.instantiateViewController(withIdentifier: "CheckoutViewController") as? CheckoutViewController
        self.navigationController?.pushViewController(checkout!, animated: false)
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    @IBAction func backbuttonAction(_ sender: Any) {
        
        _ = self.navigationController?.popViewController(animated: true)

    }
    
    func animateViewMoving (up:Bool, moveValue :CGFloat)
    {
        let movementDuration:TimeInterval = 0.3
        let movement:CGFloat = ( up ? -moveValue : moveValue)
        UIView.beginAnimations( "animateView", context: nil)
        UIView.setAnimationBeginsFromCurrentState(true)
        UIView.setAnimationDuration(movementDuration)
        self.view.frame = self.view.frame.offsetBy(dx: 0,  dy: movement)
        UIView.commitAnimations()
    }
    func textFieldDidBeginEditing(_ textField: UITextField)
    {
        if(textField.tag == 1)
        {
            animateViewMoving(up: true, moveValue: 120)
        }
        else if(textField.tag == 2)
        {
            animateViewMoving(up: true, moveValue: 120)
            
        }
        else if(textField.tag == 3)
        {
            animateViewMoving(up: true, moveValue: 120)
        }
        else if(textField.tag == 4)
        {
            
            animateViewMoving(up: true, moveValue: 120)
            tap = UITapGestureRecognizer(target: self, action: #selector(UIInputViewController.dismissKeyboard))
           self.view.addGestureRecognizer(tap)
        }
        else if(textField.tag == 5)
        {
            animateViewMoving(up: true, moveValue: 120)
        }
        else if(textField.tag == 6)
        {
            animateViewMoving(up: true, moveValue: 120)
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField)
    {
        if(textField.tag == 1)
        {
            animateViewMoving(up: false, moveValue: 120)
        }
        else if(textField.tag == 2)
        {
            animateViewMoving(up: false, moveValue: 120)
        }
        else if(textField.tag == 3)
        {
            animateViewMoving(up: false, moveValue: 120)
        }
        else if(textField.tag == 4)
        {
            animateViewMoving(up: false, moveValue: 120)
            
            self.view.removeGestureRecognizer(tap)
            
            
        }
        else if(textField.tag == 5)
        {
            animateViewMoving(up: false, moveValue: 120)
        }
        else if(textField.tag == 6)
        {
            animateViewMoving(up: false, moveValue: 120)
        }
    }
    
    func dismissKeyboard()
    {
            phoneNuber_txt?.resignFirstResponder()

            }
}
