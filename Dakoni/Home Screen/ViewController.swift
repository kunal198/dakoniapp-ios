//
//  ViewController.swift
//  Dakoni
//
//  Created by brst on 1/2/17.
//  Copyright © 2017 brst. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate,UITableViewDataSource,UITableViewDelegate,UIScrollViewDelegate {

    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var scrollImageView: UIImageView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var newArrivalCollectionView: UICollectionView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var collectionView: UICollectionView!
    
    
    // Data model: These strings will be the data for the table view cells
    @IBOutlet weak var SlideView: UIView!
    let newArrivalproductImageArray: [String] = ["new-arrival-1.png", "new-arrival2.png", "new-arrival3.png", "new-arrival2.png"]
    
    let productNameArray: [String] = ["Desktops","Mobile Phones","Tablets","Laptops","C.P.U's","Headphones","Televisions","Appliances"]
    
    let categoryProductImageArray: [String] = ["desktop-icn.png","phone-icn.png","tab-icn.png","laptop-icn.png","cpu-icn.png","headphone-icn.png","tv-icn.png","radio-icn.png"]
    
    let imageArray: [String] = ["banner-1.png","banner-2.png","banner-3.png"]
    
    // cell reuse id (cells that scroll out of view can be reused)
    let cellReuseIdentifier = "Cell"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        self.scrollView.frame = CGRect(x:0, y:10, width:self.scrollImageView.frame.width, height:self.scrollImageView.frame.height)
        let scrollViewWidth:CGFloat = self.scrollImageView.frame.width
        let scrollViewHeight:CGFloat = self.scrollImageView.frame.height
        
        let imgOne = UIImageView(frame: CGRect(x:0, y:0,width:scrollViewWidth, height:scrollViewHeight))
        imgOne.image = UIImage(named: "banner-1.png")
        let imgTwo = UIImageView(frame: CGRect(x:scrollViewWidth, y:0,width:scrollViewWidth, height:scrollViewHeight))
        imgTwo.image = UIImage(named: "banner-2.png")
        let imgThree = UIImageView(frame: CGRect(x:scrollViewWidth*2, y:0,width:scrollViewWidth, height:scrollViewHeight))
        imgThree.image = UIImage(named: "banner-3.png")
        
        self.scrollView.addSubview(imgOne)
        self.scrollView.addSubview(imgTwo)
        self.scrollView.addSubview(imgThree)
       
        //4
        self.scrollView.contentSize = CGSize(width:self.scrollImageView.frame.width * 3, height:self.scrollImageView.frame.height)
        self.scrollView.delegate = self
        self.pageControl.currentPage = 0
        
        Timer.scheduledTimer(timeInterval: 2, target: self, selector: #selector(moveToNextPage), userInfo: nil, repeats: true)
        
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        
        self.SlideView.frame.origin.x = -(self.SlideView.frame.size.width)
        //self.BlackBlurView.hidden = true
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
     func numberOfSections(in collectionView: UICollectionView) -> Int
    {
      return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        if collectionView.tag == 1 {
            return productNameArray.count
        }
        else if collectionView.tag == 2
        {
            return newArrivalproductImageArray.count
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell{
        
        var cell: CollectionViewCell = CollectionViewCell()
        
        print("hello")
        
        if collectionView.tag == 1 {
            
            cell = self.collectionView.dequeueReusableCell(withReuseIdentifier: cellReuseIdentifier, for: indexPath as IndexPath) as! CollectionViewCell
            
            let image = UIImage(named: categoryProductImageArray[indexPath.row]) as UIImage?
            cell.item_Category.image = image
            
            cell.itemName_Category.text = productNameArray[indexPath.row]
            
        }
        else if collectionView.tag == 2
        {
            
            
            cell = self.newArrivalCollectionView.dequeueReusableCell(withReuseIdentifier: cellReuseIdentifier, for: indexPath as IndexPath) as! CollectionViewCell
            
            let image = UIImage(named: newArrivalproductImageArray[indexPath.row]) as UIImage?
            cell.newArrival_ItemName.image = image
        }
        
        return cell
    }
    
    
    //Use for size
    
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        
        if collectionView.tag == 2 {
            let size = CGSize(width: self.newArrivalCollectionView.frame.size.width/3, height: self.newArrivalCollectionView.frame.size.height)
            
            return size
        }
        
        let size = CGSize(width: 40, height: 57)
        
        return size
        
    }
    func collectionView(collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        
        
        if collectionView.tag == 2 {
            let size = CGSize(width: self.newArrivalCollectionView.frame.size.width/3, height: self.newArrivalCollectionView.frame.size.height)
            
            return size
        }
        
        let size = CGSize(width: 40, height: 57)
        
        return size
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let ProductViewController = self.storyboard?.instantiateViewController(withIdentifier: "ProductViewController") as? ProductViewController
        self.navigationController?.pushViewController(ProductViewController!, animated: true)
        
        print("Cell \(indexPath.row) selected")
    }
    
    @IBAction func slidebutton(_ sender: UIButton) {
        
        UIView.animate(withDuration: 0.4, delay: 0.0, options: UIViewAnimationOptions.transitionFlipFromLeft, animations: { () -> Void in
            
            self.SlideView.frame.origin.x = 0
            
        }, completion: { (finished: Bool) -> Void in
            // you can do this in a shorter, more concise way by setting the value to its opposite, NOT value
        })
        
    }
    
    @IBAction func slidebtnonslidview(_ sender: UIButton) {
        UIView.animate(withDuration: 0.4, delay: 0.0, options: UIViewAnimationOptions.transitionFlipFromLeft, animations: { () -> Void in
            
            self.SlideView.frame.origin.x = -(self.SlideView.frame.size.width)
            
        }, completion: { (finished: Bool) -> Void in
            
            // you can do this in a shorter, more concise way by setting the value to its opposite, NOT value
            
        })
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
     return 40.0
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 9;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell:UITableViewCell = self.tableView.dequeueReusableCell(withIdentifier: String(indexPath.row))! as UITableViewCell
                return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.row == 0 {
            let homeViewController = self.storyboard?.instantiateViewController(withIdentifier: "ViewController") as? ViewController
            self.navigationController?.pushViewController(homeViewController!, animated: true)
            
        }
        else if indexPath.row == 1{
            let searchViewController = self.storyboard?.instantiateViewController(withIdentifier: "SearchViewController") as? SearchViewController
            self.navigationController?.pushViewController(searchViewController!, animated: true)
        }
        else if indexPath.row == 2{
            
            let LoginController = self.storyboard?.instantiateViewController(withIdentifier: "LoginVewController") as? LoginVewController
            self.navigationController?.pushViewController(LoginController!, animated: true)

        }
        else if indexPath.row == 3{
            let myProfileViewController = self.storyboard?.instantiateViewController(withIdentifier: "MyProfileViewController") as? MyProfileViewController
            self.navigationController?.pushViewController(myProfileViewController!, animated: true)
        }
        else if indexPath.row == 4{
            let myCartViewController = self.storyboard?.instantiateViewController(withIdentifier: "MyCartViewController") as? MyCartViewController
            self.navigationController?.pushViewController(myCartViewController!, animated: true)
        }
        else if indexPath.row == 5{
            let aboutUsViewController = self.storyboard?.instantiateViewController(withIdentifier: "AboutUsViewController") as? AboutUsViewController
            self.navigationController?.pushViewController(aboutUsViewController!, animated: true)
        }
        else if indexPath.row == 6{
            let contactUsViewController = self.storyboard?.instantiateViewController(withIdentifier: "ContactUsViewController") as? ContactUsViewController
            self.navigationController?.pushViewController(contactUsViewController!, animated: true)
        }
        else if indexPath.row == 7{
            let viewController = self.storyboard?.instantiateViewController(withIdentifier: "ViewController") as? ViewController
            self.navigationController?.pushViewController(viewController!, animated: true)
        }
        
        print("You selected cell #\(indexPath.row)!")
    }
    
    @IBAction func shoppingUpBtnAction(_ sender: Any) {
        
        collectionView.setContentOffset(CGPoint.init(x: self.collectionView.contentSize.width - self.collectionView.frame.size.width, y: collectionView.contentOffset.y), animated: true)
    }

    @IBAction func newArrivalUpBtn_Action(_ sender: Any) {
        
    collectionView.setContentOffset(CGPoint.init(x: self.collectionView.contentSize.width - self.collectionView.frame.size.width, y: collectionView.contentOffset.y), animated: true)
        
    }
    
    @IBAction func shoppingDownBtn_Action(_ sender: Any) {

        collectionView.setContentOffset(CGPoint.init(x: 0, y: collectionView.contentOffset.y), animated: true)

    }
    @IBAction func newArrivalDownBtn_Action(_ sender: Any) {
        
        collectionView.setContentOffset(CGPoint.init(x: 0, y: collectionView.contentOffset.y), animated: true)
        
     }
    
    func moveToNextPage (){
        
        let pageWidth:CGFloat = self.scrollView.frame.width
        let maxWidth:CGFloat = pageWidth * 3
        let contentOffset:CGFloat = self.scrollView.contentOffset.x
        
        var slideToX = contentOffset + pageWidth
        
        if  contentOffset + pageWidth == maxWidth{
            slideToX = 0
        }
        self.scrollView.scrollRectToVisible(CGRect(x:slideToX, y:-40, width:pageWidth, height:self.scrollImageView.frame.height), animated: true)
    }
    
    //MARK: UIScrollView Delegate
    func scrollViewDidEndScrollingAnimation(_ scrollView: UIScrollView){
        // Test the offset and calculate the current page after scrolling ends
        let pageWidth:CGFloat = scrollView.frame.width
        let currentPage:CGFloat = floor((scrollView.contentOffset.x-pageWidth/2)/pageWidth)+1
        // Change the indicator
        self.pageControl.currentPage = Int(currentPage);

            // Show the "Let's Start" button in the last slide (with a fade in animation)
            UIView.animate(withDuration: 1.0, animations: { () -> Void in
                //  self.startButton.alpha = 1.0
            })
        }
    
    @IBAction func languageBtnAction(_ sender: Any) {
        
        let LoginController = self.storyboard?.instantiateViewController(withIdentifier: "LoginVewController") as? LoginVewController
        self.navigationController?.pushViewController(LoginController!, animated: true)
    }
    
}

