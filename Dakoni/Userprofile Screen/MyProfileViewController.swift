//
//  MyProfileViewController.swift
//  Dakoni
//
//  Created by brst on 1/5/17.
//  Copyright © 2017 brst. All rights reserved.
//

import UIKit

class MyProfileViewController: UIViewController, UITableViewDelegate,UITableViewDataSource {

    @IBOutlet weak var slideView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func viewWillAppear(_ animated: Bool)
    {
        
        self.slideView.frame.origin.x = -(self.slideView.frame.size.width)
        //self.BlackBlurView.hidden = true
        
    }
    
    @IBAction func slideBtnAction(_ sender: Any) {
        UIView.animate(withDuration: 0.4, delay: 0.0, options: UIViewAnimationOptions.transitionFlipFromLeft, animations: { () -> Void in
            
            self.slideView.frame.origin.x = 0
            
        }, completion: { (finished: Bool) -> Void in
            // you can do this in a shorter, more concise way by setting the value to its opposite, NOT value
        })

    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func crossBtnAction(_ sender: Any) {
        UIView.animate(withDuration: 0.4, delay: 0.0, options: UIViewAnimationOptions.transitionFlipFromLeft, animations: { () -> Void in
            
            self.slideView.frame.origin.x = -(self.slideView.frame.size.width)
            
        }, completion: { (finished: Bool) -> Void in
            
            // you can do this in a shorter, more concise way by setting the value to its opposite, NOT value
            
        })

    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        return 40.0
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 9;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell:UITableViewCell = tableView.dequeueReusableCell(withIdentifier: String(indexPath.row))! as UITableViewCell
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.row == 0 {
            let homeViewController = self.storyboard?.instantiateViewController(withIdentifier: "ViewController") as? ViewController
            self.navigationController?.pushViewController(homeViewController!, animated: true)
            
        }
        else if indexPath.row == 1{
            let searchViewController = self.storyboard?.instantiateViewController(withIdentifier: "SearchViewController") as? SearchViewController
            self.navigationController?.pushViewController(searchViewController!, animated: true)
        }
        else if indexPath.row == 2{
            
            let LoginController = self.storyboard?.instantiateViewController(withIdentifier: "LoginVewController") as? LoginVewController
            self.navigationController?.pushViewController(LoginController!, animated: true)
            
        }
        else if indexPath.row == 3{
            let myProfileViewController = self.storyboard?.instantiateViewController(withIdentifier: "MyProfileViewController") as? MyProfileViewController
            self.navigationController?.pushViewController(myProfileViewController!, animated: true)
        }
        else if indexPath.row == 4{
            let myCartViewController = self.storyboard?.instantiateViewController(withIdentifier: "MyCartViewController") as? MyCartViewController
            self.navigationController?.pushViewController(myCartViewController!, animated: true)
        }
        else if indexPath.row == 5{
            let aboutUsViewController = self.storyboard?.instantiateViewController(withIdentifier: "AboutUsViewController") as? AboutUsViewController
            self.navigationController?.pushViewController(aboutUsViewController!, animated: true)
        }
        else if indexPath.row == 6{
            let contactUsViewController = self.storyboard?.instantiateViewController(withIdentifier: "ContactUsViewController") as? ContactUsViewController
            self.navigationController?.pushViewController(contactUsViewController!, animated: true)
        }
        else if indexPath.row == 7{
            let viewController = self.storyboard?.instantiateViewController(withIdentifier: "ViewController") as? ViewController
            self.navigationController?.pushViewController(viewController!, animated: true)
        }
        
        print("You selected cell #\(indexPath.row)!")
    }


}
