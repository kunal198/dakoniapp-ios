//
//  CollectionViewCell.swift
//  Dakoni
//
//  Created by brst on 1/4/17.
//  Copyright © 2017 brst. All rights reserved.
//

import UIKit

class CollectionViewCell: UICollectionViewCell {
    
    // Shop By Categories Outlet
    
    @IBOutlet weak var item_Category: UIImageView!
    
    @IBOutlet weak var itemName_Category: UILabel!
    
    
    // New arrivals Outlet
    
    @IBOutlet weak var newArrival_ItemName: UIImageView!
}
