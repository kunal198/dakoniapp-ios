//
//  CheckoutViewController.swift
//  Dakoni
//
//  Created by brst on 1/6/17.
//  Copyright © 2017 brst. All rights reserved.
//

import UIKit

class CheckoutViewController: UIViewController,UITextFieldDelegate,UIScrollViewDelegate {
    @IBOutlet weak var expiryDateView: UIView!

    @IBOutlet weak var guestBtn: UIButton!
    @IBOutlet weak var checkoutBtn: UIButton!
        @IBOutlet weak var securityCode_Vew: UIView!
    @IBOutlet weak var zip_postalCode_txt: UITextField!
    @IBOutlet weak var paymentDetailBackground_View: UIView!
    @IBOutlet weak var payPalView: UIView!
    @IBOutlet weak var cashOnDeliveryView: UIView!
    @IBOutlet weak var creditCardView: UIView!
    @IBOutlet weak var phoneNumber: UITextField!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet var password_TxtField: [UITextField]!
    var tap = UITapGestureRecognizer()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        checkoutBtn.imageView?.contentMode = UIViewContentMode.scaleAspectFit
        
        guestBtn.imageView?.contentMode = UIViewContentMode.scaleAspectFit
   
        expiryDateView.layer.cornerRadius = 5.0
        expiryDateView.layer.borderWidth = 0.3
        expiryDateView.layer.borderColor = UIColor.lightGray.cgColor
        expiryDateView.clipsToBounds = true
       
        securityCode_Vew.layer.cornerRadius = 5.0
        securityCode_Vew.layer.borderWidth = 0.3
        securityCode_Vew.layer.borderColor = UIColor.lightGray.cgColor
        securityCode_Vew.clipsToBounds = true

        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        self.scrollView.contentSize = CGSize(width: self.scrollView.contentSize.width, height: paymentDetailBackground_View.frame.origin.x + paymentDetailBackground_View.frame.size.height + 50)
    
        print(self.scrollView.frame.size.height)
        print(self.scrollView.contentSize.height)
    
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func creditCardbtn_Action(_ sender: Any) {
        self.cashOnDeliveryView.isHidden = true
        self.payPalView.isHidden = true
        self.creditCardView.isHidden = false
    }

    @IBAction func backBtnAction(_ sender: Any) {
        _ = self.navigationController?.popViewController(animated: true)

    }
    
    @IBAction func payPalBtn_Action(_ sender: Any) {
        
        self.cashOnDeliveryView.isHidden = true
        self.payPalView.isHidden = false
        self.creditCardView.isHidden = true
    }

    @IBAction func cashOnDeliveryBtn_Action(_ sender: Any) {
        
        self.cashOnDeliveryView.isHidden = false
        self.payPalView.isHidden = true
        self.creditCardView.isHidden = true
        
    }
    @IBAction func forGotPassword_Btn_Action(_ sender: Any) {
        
        let forgotPasswordView = self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as? ForgotPasswordViewController
        self.navigationController?.pushViewController(forgotPasswordView!, animated: true)
        
    }
    @IBAction func loginPayPalBtn_Action(_ sender: Any) {
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    func animateViewMoving (up:Bool, moveValue :CGFloat)
    {
        let movementDuration:TimeInterval = 0.3
        let movement:CGFloat = ( up ? -moveValue : moveValue)
        UIView.beginAnimations( "animateView", context: nil)
        UIView.setAnimationBeginsFromCurrentState(true)
        UIView.setAnimationDuration(movementDuration)
        self.view.frame = self.view.frame.offsetBy(dx: 0,  dy: movement)
        UIView.commitAnimations()
    }
    func textFieldDidBeginEditing(_ textField: UITextField)
    {
        if(textField.tag == 7)
        {
            animateViewMoving(up: true, moveValue: 120)
        }
        else if(textField.tag == 8)
        {
            animateViewMoving(up: true, moveValue: 120)
            
        }
        else if(textField.tag == 9)
        {
            animateViewMoving(up: true, moveValue: 120)
        }
        else if(textField.tag == 10)
        {
            
            animateViewMoving(up: true, moveValue: 120)
            tap = UITapGestureRecognizer(target: self, action: #selector(UIInputViewController.dismissKeyboard))
            self.view.addGestureRecognizer(tap)
        }
        else if(textField.tag == 11)
        {
            animateViewMoving(up: true, moveValue: 120)
        }
        else if(textField.tag == 12)
        {
            animateViewMoving(up: true, moveValue: 120)
        }
        else if(textField.tag == 13)
        {
            animateViewMoving(up: true, moveValue: 120)
            tap = UITapGestureRecognizer(target: self, action: #selector(UIInputViewController.dismissKeyboard))
            self.view.addGestureRecognizer(tap)
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField)
    {
        if(textField.tag == 7)
        {
            animateViewMoving(up: false, moveValue: 120)
        }
        else if(textField.tag == 8)
        {
            animateViewMoving(up: false, moveValue: 120)
        }
        else if(textField.tag == 9)
        {
            animateViewMoving(up: false, moveValue: 120)
        }
        else if(textField.tag == 10)
        {
            
            animateViewMoving(up: false, moveValue: 120)
            self.view.removeGestureRecognizer(tap)
            
        }
        else if(textField.tag == 11)
        {
            animateViewMoving(up: false, moveValue: 120)
        }
        else if(textField.tag == 12)
        {
            animateViewMoving(up: false, moveValue: 120)
        }
        else if(textField.tag == 13)
        {
            animateViewMoving(up: false, moveValue: 120)
            self.view.removeGestureRecognizer(tap)
        }
    }
    
    func dismissKeyboard()
    {
        phoneNumber?.resignFirstResponder()
        zip_postalCode_txt?.resignFirstResponder()
    }
    
    @IBAction func guestCheckout_Action(_ sender: Any) {
    }
    @IBAction func checkoutBtn_action(_ sender: Any) {
    }

}
