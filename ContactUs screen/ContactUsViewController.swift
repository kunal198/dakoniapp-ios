//
//  ContactUsViewController.swift
//  Dakoni
//
//  Created by mrinal khullar on 1/4/17.
//  Copyright © 2017 brst. All rights reserved.
//

import UIKit


class ContactUsViewController: UIViewController {

    @IBOutlet var addressLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

     }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func backButtonAction(_ sender: Any) {
        
        _ = self.navigationController?.popViewController(animated: true)

    }

  }
