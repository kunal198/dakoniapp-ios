//
//  ProductViewController.swift
//  Dakoni
//
//  Created by brst on 1/5/17.
//  Copyright © 2017 brst. All rights reserved.
//

import UIKit

class ProductViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    // Data model: These strings will be the data for the table view cells
    let productImageArray: [String] = ["Headphones.png", "product-1.png", "product-2.png", "product-3.png"]
    
    // cell reuse id (cells that scroll out of view can be reused)
    let cellReuseIdentifier = "Cell"
    
    @IBOutlet var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return productImageArray.count;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell:TableViewCell = self.tableView.dequeueReusableCell(withIdentifier: "Cell")! as UITableViewCell as! TableViewCell
        let image = UIImage(named: productImageArray[indexPath.row]) as UIImage?
        cell.item_Category_imgVew.image = image
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("You selected cell #\(indexPath.row)!")
    }
    @IBAction func checkOutbtn_Action(_ sender: Any) {
        
        let checkout = self.storyboard?.instantiateViewController(withIdentifier: "shippingInformationViewController") as? shippingInformationViewController
        self.navigationController?.pushViewController(checkout!, animated: true)
        
    }
    
    @IBAction func addToCartBtn_Action(_ sender: Any) {
        
        let alert = UIAlertController(title: "Alert", message: "Item Added to Cart", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.default, handler: {(action:UIAlertAction!) in print("you have pressed the Cancel button")
        }))
        self.present(alert, animated: true, completion: nil)
    
    }
    
    @IBAction func backBtnAction(_ sender: Any) {
        _ = self.navigationController?.popViewController(animated: true)

    }
    
    @IBAction func wishlistBtnAction(_ sender: Any) {
        
        let alert = UIAlertController(title: "Alert", message: "Item Added to WishLIst", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.default, handler: {(action:UIAlertAction!) in print("you have pressed the Cancel button")
        }))
        self.present(alert, animated: true, completion: nil)    }
}
